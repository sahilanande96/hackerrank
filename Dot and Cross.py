import numpy

dimensions = int(input())
data_A = []
data_B = []
for _ in range(dimensions):
    temp = list(map(int, (input().split())))
    data_A.append(temp)
    
for j in range(dimensions):
    temp = list(map(int, (input().split())))
    data_B.append(temp)
    
numpy_data_A = data_A
numpy_data_B = data_B
print(numpy.dot(numpy_data_A, numpy_data_B))