# Enter your code here. Read input from STDIN. Print output to STDOUT

dim = input();
o = dim.split()
row = int(o[0])
col = int(o[1])

for i in range(0, int(row/2)):
    pattern = ".|."  * (2*i + 1)
    print(pattern.center(col,'-'))

print("WELCOME".center(col,'-'))


rev = int(row/2)

while rev > 0:
    pattern = ".|."  * (2*rev - 1)
    print(pattern.center(col,'-'))
    rev -= 1
