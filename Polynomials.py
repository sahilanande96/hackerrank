import numpy

values_P = list(map(float, input().split()))
value_x = float(input())
print(numpy.polyval(values_P, value_x))
