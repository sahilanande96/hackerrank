import numpy


dim = int(input())
matrix = []
for _ in range(dim):
    matrix.append(list(map(float, input().split())))
    
print(round(numpy.linalg.det(matrix),2))
