#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solve(s):
    words = s.split(" ")
    l = len(words)
    str =""
    for i in range(l):
        rep = words[i].capitalize()
        str += rep 
        str += " "
    return str
    

if __name__ == '__main__':