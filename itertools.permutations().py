# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import permutations

X = input().split(" ")

for i in sorted(permutations(X[0],int(X[1]))):
    print("".join(i))
    

