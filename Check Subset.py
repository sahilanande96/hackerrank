# Enter your code here. Read input from STDIN. Print output to STDOUT
number_sets = int(input())

for _ in range(number_sets):
    elements_A = int(input())
    set_A = set(input().split())

    elements_B = int(input())
    set_B = set(input().split())

    result = set_B.union(set_A)

    if result == set_B:
        print(True)
    else:
        print(False)





