# Enter your code here. Read input from STDIN. Print output to STDOUT
import re
count = int(input())
for _ in range(count):
    print(bool(re.match(r"^[+-]?[0-9]*\.[0-9]+$",input())))
