# Enter your code here. Read input from STDIN. Print output to STDOUT
import sys 

set_A = set(map(int, input().split()))
compare = set_A.union(set())

n = int(input())

elements = set()
for _ in range(n):
    elements.add(map(int, input().split()))

for i in elements:
    set_A.update(i)

    if compare != set_A:
        print(False)
        sys.exit()
    else:
        continue

print(True)