def mutate_string(string, position, character):
    converted_list = list(string)
    converted_list[position] = character
    s_new = "".join(converted_list)
    return s_new

if __name__ == '__main__':
    s = input()
    i, c = input().split()
    s_new = mutate_string(s, int(i), c)
    print(s_new)