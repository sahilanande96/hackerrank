# Enter your code here. Read input from STDIN. Print output to STDOUT
nm = input().split(" ")
ele_n = input().split(" ")
ele_A = input().split(" ")
ele_B = input().split(" ")
ele_AA = set(ele_A)
ele_BB = set(ele_B)
happiness = 0
for x in ele_n:
    if x in ele_AA:
        happiness+=1
    elif x in ele_BB:
        happiness-=1
    else:
        continue

print(happiness)
