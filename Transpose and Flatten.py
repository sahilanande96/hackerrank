import numpy



dimensions = list(map(int,(input().split())))
# print(dimensions)
data = []
for _ in range(dimensions[0]):
    temp = list(map(int,(input().split())))
    data.append(temp)
    
# print(data)
numpy_data = numpy.array(data)
print(numpy.transpose(numpy_data))
print(numpy_data.flatten())    

