import numpy

dimensions = list(map(int, (input().split())))
data_A = []
data_B = []
# print(dimensions)
for  _ in range(dimensions[0]):
    temp = list(map(int, (input().strip().split())))
    data_A.append(temp)
    
# print(data_A)
for j in range(dimensions[0]):
    temp = list(map(int, (input().strip().split())))
    data_B.append(temp)
    
numpy_data_A = numpy.array(data_A)
numpy_data_B = numpy.array(data_B)

print(numpy_data_A + numpy_data_B)
print(numpy_data_A - numpy_data_B)
print(numpy_data_A * numpy_data_B)
print(numpy_data_A // numpy_data_B)
print(numpy_data_A % numpy_data_B)
print(numpy_data_A ** numpy_data_B)
