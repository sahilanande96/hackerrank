if __name__ == '__main__':
    N = int(input())
    result = []

    for i in range(N):
        function, *data = input().split()
        if function == "insert":
            
            result.insert(int(data[0]),int(data[1]))
        elif function == "remove":
            result.remove(int(data[0]))
        elif function == "append":
            result.append(int(data[0]))
        elif function == "sort":
            result.sort()
        elif function == "pop":
            result.pop()
        elif function == "reverse":
            result.reverse()
        elif function == "print":
            print(result)
        else:
            print("Please enter correct function.")
