def swap_case(s):
    result = ""
    for alphabet in s:
        if alphabet == alphabet.upper():
            result += alphabet.lower()
        else:
            result += alphabet.upper()
    return result

if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)