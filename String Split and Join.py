def split_and_join(line):
    # write your code here
    X = line.split(" ")
    result = "-".join(X)
    return result

if __name__ == '__main__':
    line = input()
    result = split_and_join(line)
    print(result)