import numpy

numpy.set_printoptions(legacy = '1.13')
dimensions = list(map(int, (input().split())))
numpy_dimensions = numpy.array(dimensions)
print(numpy.eye(numpy_dimensions[0], numpy_dimensions[1]))


