import numpy

dimensions = list(map(int,(input().split())))
# print(dimensions)
data_A = [] 
data_B = []
for _ in range(dimensions[0]):
    temp = list(map(int,(input().split())))
    data_A.append(temp)
    
for j in range(dimensions[1]):
    temp = list(map(int,(input().split())))
    data_B.append(temp)
    
# print(data_A)
# print(data_B)
numpy_data_A = numpy.array(data_A)
numpy_data_B = numpy.array(data_B)


print(numpy.concatenate((numpy_data_A, numpy_data_B), axis = 0))