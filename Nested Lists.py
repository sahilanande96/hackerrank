if __name__ == '__main__':
    name_list = []
    score_list = []
    for _ in range(int(input())):
        name = input()
        score = float(input())

        name_list.append(name)
        score_list.append(score)
    
    #Check the minimum score
    min_score = min(score_list)
    
    occurence = score_list.count(min_score)

    for j in range(occurence):
        #Remove that minimum score as well as the name it is associated with
        index_min_score = score_list.index(min_score)
        score_list.remove(min_score)
        name_list.pop(index_min_score)

    #This minimum score is actually the second lowest score
    min_score = min(score_list)

    #Check how many times it occures
    occurence = score_list.count(min_score)

    result_name = []
    for i in range(occurence):
        index_min_score = score_list.index(min_score)
        result_name.append(name_list.pop(index_min_score))
        score_list.remove(min_score)

    result_name.sort()

    for x in result_name:
        print(x)



