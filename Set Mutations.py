# Enter your code here. Read input from STDIN. Print output to STDOUT
number_elements_A = int(input())
elements_A = set(map(int, input().split()))

number_other_sets = int(input())

for _ in range(number_other_sets):
    command = input().split(" ")

    elements_sets = set(map(int, input().split()))

    if command[0] == "update":
        elements_A.update(elements_sets)
    elif command[0] == "intersection_update":
        elements_A.intersection_update(elements_sets)
    elif command[0] == "symmetric_difference_update":
        elements_A.symmetric_difference_update(elements_sets)
    else:
        elements_A.difference_update(elements_sets)

print(sum(elements_A))
