import numpy

# numpy.set_printoptions(legacy='1.13')
dimensions = list(map(int, (input().split())))
data = []
for _ in range(dimensions[0]):
    temp = list(map(int, input().split()))
    data.append(temp)

print(numpy.mean(data, axis = 1))
print(numpy.var(data, axis = 0))
std = numpy.std(data)
print(numpy.around(std, 11))
