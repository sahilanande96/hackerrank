# Enter your code here. Read input from STDIN. Print output to STDOUT
#from collections import namedtuple

total_student = int(input())
column_names = input().split()
#entries = namedtuple('entries', 'column_0 column_1 column_2 column_3')

marks = column_names.index("MARKS")
marks_list = []
addition = 0

for _ in range(total_student):
    xyz = input().split()
    marks_list.append(xyz[marks])

for i in marks_list:
    addition += int(i)

print("{:.2f}".format(addition/total_student))
