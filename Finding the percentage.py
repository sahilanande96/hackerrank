if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()

    if student_marks.get(query_name) != None:
        score_list = student_marks[query_name]
        result = sum(score_list)/len(score_list)
        print('%.2f'%result)
