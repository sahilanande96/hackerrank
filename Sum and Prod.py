import numpy

dimensions = list(map(int, (input().split())))
data = []
for _ in range(dimensions[0]):
    temp = list(map(int, input().split()))
    data.append(temp)

numpy_data_A = numpy.array(data)

print(numpy.prod(numpy.sum(data, axis = 0)))