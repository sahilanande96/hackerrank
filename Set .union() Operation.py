# Enter your code here. Read input from STDIN. Print output to STDOUT
ne = int(input())
nne = set(map(int, input().split()))

nf = int(input())
nnf = set(map(int, input().split()))

print(len(nne.union(nnf)))
