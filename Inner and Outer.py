import numpy

data_A = numpy.array(list(map(int, (input().split()))))
data_B = numpy.array(list(map(int, (input().split()))))


print(numpy.inner(data_A, data_B))
print(numpy.outer(data_A, data_B))