import numpy

dimensions = list(map(int, (input().split())))
data = []
for _ in range(dimensions[0]):
    temp = list(map(int, input().split()))
    data.append(temp)

print(numpy.max(numpy.min(data, axis = 1)))