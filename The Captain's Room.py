# Enter your code here. Read input from STDIN. Print output to STDOUT
k = int(input())
result =set()
repeat = set()

for _ in (input().split(" ")):
    if _ not in result:
        result.add(_)
    else:
        repeat.add(_)

result.difference_update(repeat)
for i in result:
    print(i)
